# Issue Tracker

Simple issue tracking tool.  
Backend: NodeJS  
Frontend: Angular (I went with something I'm **comfortable** with. I wasn't sure if I will be able to learn React in such a short time and decided to play it safe.)

### Server  
`cd issue-tracker-server`  
`npm install`  
Testing: `npm test`  
Running: `npm start` - it will run on port 3000

### Client
`cd issue-tracker-client`  
`npm install`  
Testing: `npm test`  
Running: `npm start` - it will run on port 4200 and automatically open `http://localhost:4200` in a default browser

#### Things for the future
* add a proper Database
* validation and error handling
* RWD
* turn it into a proper PWA with push notifications etc.
* authorization and RBAC
* filtering, sorting, pagination of the main table
* comments/discussions for issues
* labels
* graphical redesign - currently there are just a default material blocks glued together
* and of course a React version ;-)
