import {Pipe, PipeTransform} from '@angular/core';
import {State} from './state';

@Pipe({
  name: 'state'
})
export class StatePipe implements PipeTransform {

  transform(value: State, args?: any): any {
    return State[value];
  }

}
