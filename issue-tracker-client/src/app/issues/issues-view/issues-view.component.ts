import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Params, Router} from '@angular/router';
import {IssuesService} from '../issues.service';
import {switchMap} from 'rxjs/operators';
import {Issue} from '../issue';

@Component({
  selector: 'app-issues-view',
  templateUrl: './issues-view.component.html',
  styleUrls: ['./issues-view.component.less']
})
export class IssuesViewComponent implements OnInit {

  public issue: Issue = null;

  constructor(private route: ActivatedRoute, private router: Router, private service: IssuesService) {
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.service.getOne(parseInt(params.id)).subscribe((issue: Issue) => {
        this.issue = issue;
      }, (error: any) => {
        this.router.navigate(['page-not-found']);
      });
    });
  }

}
