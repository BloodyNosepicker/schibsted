import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IssuesViewComponent} from './issues-view.component';
import {MaterialModule} from '../../material/material.module';
import {StatePipe} from '../state.pipe';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('IssuesViewComponent', () => {
  let component: IssuesViewComponent;
  let fixture: ComponentFixture<IssuesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IssuesViewComponent, StatePipe],
      imports: [MaterialModule, RouterTestingModule, HttpClientTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
