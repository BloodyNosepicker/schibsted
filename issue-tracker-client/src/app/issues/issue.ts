import {State} from './state';

export class Issue {

  constructor(
    private _id: number,
    private _title: string,
    private _description: string,
    private _state: State,
    private _createdAt: string) {
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get state(): State {
    return this._state;
  }

  set state(value: State) {
    this._state = value;
  }

  get createdAt(): string {
    return this._createdAt;
  }

  set createdAt(value: string) {
    this._createdAt = value;
  }

  public static empty(): Issue {
    return new Issue(null, '', '', State.Open, '');
  }

  public static fromJson(json: any): Issue {
    return new Issue(
      json.id,
      json.title,
      json.description,
      json.state,
      json.createdAt
    );
  }

  public toJson(): any {
    return {
      id: this.id,
      title: this.title,
      description: this.description,
      state: this.state,
      createdAt: this.createdAt
    };
  }

  public canBePending(): boolean {
    return this._state === State.Open;
  }

  public canBeClosed(): boolean {
    return this._state !== State.Closed;
  }
}
