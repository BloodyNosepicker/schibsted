import {StatePipe} from './state.pipe';
import {State} from './state';

describe('StatePipe', () => {
  it('create an instance', () => {
    const pipe = new StatePipe();
    expect(pipe).toBeTruthy();
  });

  it('should return proper state name', () => {
    const pipe = new StatePipe();
    expect(pipe.transform(State.Open)).toBe('Open');
    expect(pipe.transform(State.Closed)).toBe('Closed');
    expect(pipe.transform(State.Pending)).toBe('Pending');
  });

  it('should fail', () => {
    const pipe = new StatePipe();
    expect(pipe.transform(null)).toBeFalsy();
  });
});
