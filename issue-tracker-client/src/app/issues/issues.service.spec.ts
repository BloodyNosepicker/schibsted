import {IssuesService} from './issues.service';
import {Issue} from './issue';
import {State} from './state';
import {defer} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';

export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

describe('IssuesService', () => {

  let httpClientSpy: {
    get: jasmine.Spy,
    post: jasmine.Spy,
    put: jasmine.Spy,
    delete: jasmine.Spy
  };
  let issuesService: IssuesService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
    issuesService = new IssuesService(<any> httpClientSpy);
  });

  it('should return all issues', () => {
    const expectedIssues: Issue[] = [
      new Issue(1, 'title', 'description', State.Open, '2018-08-04 22:00:00'),
      new Issue(2, 'title2', 'description2', State.Pending, '2018-08-04 22:00:00'),
      new Issue(3, 'title3', 'description3', State.Closed, '2018-08-04 22:00:00')
    ];

    httpClientSpy.get.and.returnValue(asyncData(expectedIssues));

    issuesService.get().subscribe(
      issues => expect(issues).toEqual(expectedIssues, 'expected issues'),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('should return one issue', () => {
    const expectedIssue: Issue = new Issue(1, 'title', 'description', State.Open, '2018-08-04 22:00:00');

    httpClientSpy.get.and.returnValue(asyncData(expectedIssue));

    issuesService.getOne(1).subscribe(
      issue => expect(issue).toEqual(expectedIssue, 'expected issue'),
      fail
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('should return 404', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'error 404',
      status: 404,
      statusText: 'Not found'
    });

    httpClientSpy.get.and.returnValue(asyncData(errorResponse));

    issuesService.getOne(1).subscribe(
      () => {
      },
      error => expect(error).toEqual(errorResponse, 'expected error')
    );
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('should create an issue', () => {
    const newIssue: Issue = new Issue(1, 'title', 'description', State.Open, '2018-08-04 22:00:00');

    httpClientSpy.post.and.returnValue(asyncData(newIssue));

    issuesService.create(newIssue).subscribe(
      issue => expect(issue).toEqual(newIssue, 'expected issue'),
      fail
    );
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call');
  });

  it('should not create an issue and return error', () => {
    const newIssue: Issue = new Issue(1, 'title', 'description', State.Open, '2018-08-04 22:00:00');
    const errorResponse = new HttpErrorResponse({
      error: 'error 400',
      status: 400,
      statusText: 'Bad request'
    });
    httpClientSpy.post.and.returnValue(asyncData(errorResponse));

    issuesService.create(newIssue).subscribe(
      () => {
      },
      error => expect(error).toEqual(errorResponse, 'expected error')
    );
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call');
  });

  it('should update an issue', () => {
    const newIssue: Issue = new Issue(1, 'title', 'description', State.Open, '2018-08-04 22:00:00');

    httpClientSpy.put.and.returnValue(asyncData(newIssue));

    issuesService.update(newIssue).subscribe(
      issue => expect(issue).toEqual(newIssue, 'expected issue'),
      fail
    );
    expect(httpClientSpy.put.calls.count()).toBe(1, 'one call');
  });

  it('should not update an issue and return error', () => {
    const newIssue: Issue = new Issue(1, 'title', 'description', State.Open, '2018-08-04 22:00:00');
    const errorResponse = new HttpErrorResponse({
      error: 'error 400',
      status: 400,
      statusText: 'Bad request'
    });
    httpClientSpy.put.and.returnValue(asyncData(errorResponse));

    issuesService.update(newIssue).subscribe(
      () => {
      },
      error => expect(error).toEqual(errorResponse, 'expected error')
    );
    expect(httpClientSpy.put.calls.count()).toBe(1, 'one call');
  });

  it('should delete an issue', () => {
    httpClientSpy.delete.and.returnValue(asyncData({}));

    issuesService.delete(1).subscribe(
      (result) => expect(result).toEqual(true),
      fail
    );
    expect(httpClientSpy.delete.calls.count()).toBe(1, 'one call');
  });

  it('should not delete an issue and return false', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'error 400',
      status: 400,
      statusText: 'Bad request'
    });
    httpClientSpy.delete.and.returnValue(asyncData(errorResponse));

    issuesService.delete(1).subscribe(
      () => {
      },
      (result) => expect(result).toEqual(false),
    );
    expect(httpClientSpy.delete.calls.count()).toBe(1, 'one call');
  });

});
