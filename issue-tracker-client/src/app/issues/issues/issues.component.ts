import {Component, OnInit} from '@angular/core';
import {Issue} from '../issue';
import {IssuesService} from '../issues.service';
import {MatDialog} from '@angular/material';
import {IssuesDeleteComponent} from '../issues-delete/issues-delete.component';
import {State} from '../state';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.less']
})
export class IssuesComponent implements OnInit {

  public issues: Issue[] = [];
  public columns: string[] = ['title', 'state', 'createdAt', 'changeState', 'actions'];
  public state = State;
  public loading = true;

  constructor(private service: IssuesService, private dialog: MatDialog) {
  }

  ngOnInit() {
    this.getIssues();
  }

  public showRemoveDialog(id: number): void {
    const dialog = this.dialog.open(IssuesDeleteComponent, {
      data: {id: id},
    });
    dialog.afterClosed().subscribe(result => {
      if (result) {
        this.service.delete(id).subscribe((response: boolean) => {
          if (response) {
            this.getIssues();
          }
        });
      }
    });
  }

  public setState(issue: Issue, state: State) {
    this.loading = true;
    issue.state = state;
    this.service.update(issue).subscribe(() => {
      this.getIssues();
    });
  }

  private getIssues(): void {
    this.loading = true;
    this.service.get().subscribe((response: Issue[]) => {
      this.issues = response;
      this.loading = false;
    });
  }

}
