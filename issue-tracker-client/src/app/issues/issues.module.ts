import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IssuesComponent} from './issues/issues.component';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '../material/material.module';
import {StatePipe} from './state.pipe';
import {HttpClientModule} from '@angular/common/http';
import {IssuesDeleteComponent} from './issues-delete/issues-delete.component';
import {IssuesViewComponent} from './issues-view/issues-view.component';
import {IssuesCreateComponent} from './issues-create/issues-create.component';
import {FormsModule} from '@angular/forms';

const issuesRoutes: Routes = [
  {path: '', component: IssuesComponent},
  {path: 'issues/create', component: IssuesCreateComponent},
  {path: 'issues/:id', component: IssuesViewComponent},
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forRoot(
      issuesRoutes
    ),
    MaterialModule,
    FormsModule
  ],
  declarations: [IssuesComponent, StatePipe, IssuesDeleteComponent, IssuesViewComponent, IssuesCreateComponent],
  entryComponents: [IssuesDeleteComponent]
})
export class IssuesModule {
}
