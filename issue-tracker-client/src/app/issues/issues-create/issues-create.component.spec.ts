import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IssuesCreateComponent} from './issues-create.component';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '../../material/material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('IssuesCreateComponent', () => {
  let component: IssuesCreateComponent;
  let fixture: ComponentFixture<IssuesCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IssuesCreateComponent],
      imports: [FormsModule, MaterialModule, HttpClientTestingModule, RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
