import {Component, OnInit} from '@angular/core';
import {Issue} from '../issue';
import {IssuesService} from '../issues.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-issues-create',
  templateUrl: './issues-create.component.html',
  styleUrls: ['./issues-create.component.less']
})
export class IssuesCreateComponent implements OnInit {

  public issue: Issue = Issue.empty();

  constructor(private service: IssuesService, private router: Router) {
  }

  ngOnInit() {
  }

  public createIssue(): void {
    if (this.validateIssue()) {
      this.service.create(this.issue).subscribe((issue: Issue) => {
        this.router.navigate(['issues', issue.id]);
      });
    }
  }

  private validateIssue(): boolean {
    // todo: validate issue
    return true;
  }

}
