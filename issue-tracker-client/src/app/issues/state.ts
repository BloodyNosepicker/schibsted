export enum State {
  Open = 0,
  Pending = 1,
  Closed = 2
}
