import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-issues-delete',
  templateUrl: './issues-delete.component.html',
  styleUrls: ['./issues-delete.component.less']
})
export class IssuesDeleteComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
  }

}
