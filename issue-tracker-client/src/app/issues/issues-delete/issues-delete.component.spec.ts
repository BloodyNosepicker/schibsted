import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IssuesDeleteComponent} from './issues-delete.component';
import {MaterialModule} from '../../material/material.module';
import {MAT_DIALOG_DATA} from '@angular/material';

describe('IssuesDeleteComponent', () => {
  let component: IssuesDeleteComponent;
  let fixture: ComponentFixture<IssuesDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IssuesDeleteComponent],
      imports: [MaterialModule],
      providers: [
        {provide: MAT_DIALOG_DATA, useValue: {}}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuesDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
