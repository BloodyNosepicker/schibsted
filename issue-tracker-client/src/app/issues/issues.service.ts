import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Issue} from './issue';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IssuesService {

  private url = '/api/v1/issues';

  constructor(private http: HttpClient) {
  }

  public create(issue: Issue): Observable<Issue> {
    return Observable.create((obs: any) => {
      this.http.post(this.url, issue.toJson()).subscribe((response: any) => {
        obs.next(Issue.fromJson(response));
        obs.complete();
      }, (error: HttpErrorResponse) => {
        obs.error(error);
      });
    });
  }

  public get(): Observable<Issue[]> {
    return Observable.create((obs: any) => {
      this.http.get(this.url).subscribe((response: any) => {
        const issues: Issue[] = [];
        for (const issue of response) {
          issues.push(Issue.fromJson(issue));
        }
        obs.next(issues);
        obs.complete();
      }, (error: HttpErrorResponse) => {
        obs.error(error);
      });
    });
  }

  public getOne(id: number): Observable<Issue> {
    return Observable.create((obs: any) => {
      this.http.get(this.url + '/' + id).subscribe((response: any) => {
        obs.next(Issue.fromJson(response));
        obs.complete();
      }, (error: HttpErrorResponse) => {
        obs.error(error);
      });
    });
  }

  public update(issue: Issue): Observable<Issue> {
    return Observable.create((obs: any) => {
      this.http.put(this.url + '/' + issue.id, issue.toJson()).subscribe((response: any) => {
        obs.next(Issue.fromJson(response));
        obs.complete();
      }, (error: HttpErrorResponse) => {
        obs.error(error);
      });
    });
  }

  public delete(id: number): Observable<boolean> {
    return Observable.create((obs: any) => {
      this.http.delete(this.url + '/' + id).subscribe(() => {
        obs.next(true);
        obs.complete();
      }, () => {
        obs.next(false);
        obs.complete();
      });
    });
  }
}
