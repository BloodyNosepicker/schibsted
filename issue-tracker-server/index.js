const express = require('express');
const bodyParser = require('body-parser');
const issues = require('./routes/issues');
const app = express();

app.use(bodyParser.json());

app.use('/api/v1/issues', issues);

const server = app.listen(3000, () => {
    console.log('Issue Tracker Server started! Listening at port 3000...');
});

module.exports = server;