let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
const model = require('../models/issue');

chai.use(chaiHttp);

describe('IssueRoutes', () => {

    const url = '/api/v1/issues';

    describe('/GET /api/v1/issues', () => {
        it('should return all issues', (done) => {
            chai.request(server)
                .get(url)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(6);
                    done();
                });
        });
    });

    describe('/GET /api/v1/issues/:id', () => {
        it('should return one issue', (done) => {
            chai.request(server)
                .get(url + '/1')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('Object');
                    res.body.id.should.equal(1);
                    res.body.title.should.equal('Lorem ipsum dolor sit amet');
                    done();
                });
        });

        it('should return 404', (done) => {
            chai.request(server)
                .get(url + '/10')
                .end((err, res) => {
                    res.should.have.status(404);
                    res.body.should.equal('Not found');
                    done();
                });
        });
    });

    describe('/POST /api/v1/issues', () => {
        it('should create new issue', (done) => {
            const issue = new model.Issue(null, 'title', 'description', 0, '');
            chai.request(server)
                .post(url)
                .send(issue)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.title.should.equal(issue.title);
                    res.body.description.should.equal(issue.description);
                    res.body.state.should.equal(issue.state);
                    res.body.createdAt.should.not.equal(issue.createdAt); //generated on insert
                    done();
                });
        });

        it('should return 400', (done) => {
            // todo: during implementation of input validation
            done();
        });
    });

    describe('/PUT /api/v1/issues/:id', () => {
        it('should update issue', (done) => {
            const issue = new model.Issue(1, 'Lorem ipsum', 'description', 1, '');
            chai.request(server)
                .put(url + '/1')
                .send(issue)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.title.should.equal(issue.title);
                    res.body.description.should.equal(issue.description);
                    res.body.state.should.equal(issue.state);
                    res.body.createdAt.should.not.equal(issue.createdAt); //not changing
                    done();
                });
        });

        it('should return 400 - issue not found', (done) => {
            const issue = new model.Issue(10, 'Lorem ipsum', 'description', 1, '');
            chai.request(server)
                .put(url + '/10')
                .send(issue)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.equal('Bad request');
                    done();
                });
        });

        it('should return 400 - incorrect input', (done) => {
            // todo: during implementation of input validation
            done();
        });
    });

    describe('/DELETE /api/v1/issues/:id', () => {
        it('should delete issue', (done) => {
            chai.request(server)
                .delete(url + '/1')
                .end((err, res) => {
                    res.should.have.status(204);
                    done();
                });
        });

        it('should return 400', (done) => {
            chai.request(server)
                .delete(url + '/10')
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.equal('Bad request');
                    done();
                });
        });
    });

    after(() => {
        server.close();
    });
});