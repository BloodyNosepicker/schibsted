const express = require('express');
const router = express.Router();
const service = require('../services/issue.service');

router.route('/').get((req, res) => {
    res.status(200).json(service.getAll());
});

router.route('/:id').get((req, res) => {
    const id = parseInt(req.params['id']);
    const issue = service.getOne(id);
    if (issue) {
        res.status(200).json(issue);
    } else {
        res.status(404).json('Not found');
    }
});

router.route('/').post((req, res) => {
    const newIssue = service.create(req.body);
    if (newIssue) {
        res.status(201).json(newIssue);
    } else {
        res.status(400).json('Bad request');
    }
});

router.route('/:id').put((req, res) => {
    const id = parseInt(req.params['id']);
    const updatedIssue = service.update(id, req.body);
    if (updatedIssue) {
        res.status(200).json(updatedIssue);
    } else {
        // todo: return 404 when issue is not found?
        res.status(400).json('Bad request');
    }
});

router.route('/:id').delete((req, res) => {
    const id = parseInt(req.params['id']);
    if (service.delete(id)) {
        res.status(204).json();
    } else {
        // todo: return 404 when issue is not found?
        res.status(400).json('Bad request');
    }
});

module.exports = router;