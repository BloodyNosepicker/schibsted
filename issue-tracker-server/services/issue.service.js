const model = require('../models/issue');
const moment = require('moment');

module.exports = new function(){
    this.issues = [
        new model.Issue(1, 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin porttitor id diam finibus congue. Etiam felis ligula, rutrum vulputate cursus et, aliquet ac lectus. Quisque et diam et nulla pretium porta in et ex. Duis a velit tortor. Etiam interdum gravida ante, quis porttitor orci sagittis ut. Suspendisse potenti. Suspendisse potenti. Ut in velit vitae diam aliquam bibendum.', 0, '2018-08-04 11:20:00'),
        new model.Issue(2, 'Quisque et diam et nulla pretium porta in et ex', 'Donec laoreet vitae turpis in bibendum. Maecenas non nisi in purus scelerisque iaculis. Suspendisse posuere et ligula et dapibus. Praesent ultricies eget sem faucibus eleifend. Integer dapibus dui in justo congue, at commodo quam efficitur. Praesent pulvinar, dolor vel sagittis efficitur, ex mauris condimentum velit, condimentum lacinia risus eros sit amet lacus. Praesent hendrerit nec nisl in finibus. Nulla ullamcorper nulla ac dignissim eleifend. Mauris condimentum diam at leo rutrum, in ornare orci consequat. Donec in mattis nunc, eget fermentum dolor.', 0, '2018-08-04 11:20:00'),
        new model.Issue(3, 'Donec laoreet vitae turpis in bibendum', 'Suspendisse ullamcorper eget nunc vel accumsan. Morbi sapien libero, lobortis eu laoreet quis, tristique a nisi. Phasellus rhoncus lorem ut consequat fermentum. Duis a vehicula ligula. Mauris non tellus eget arcu iaculis tempor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam diam mauris, feugiat id odio sit amet, luctus tincidunt neque. Donec fermentum, tellus id semper aliquam, augue nulla porta erat, sit amet cursus metus dui eget tortor. Integer sodales justo sem, et pharetra leo pellentesque sed. Morbi venenatis sapien sed purus varius, nec auctor arcu rhoncus. Sed et diam sem.', 0, '2018-08-04 11:20:00'),
        new model.Issue(4, 'Suspendisse ullamcorper eget nunc vel accumsan', 'Vestibulum rhoncus purus eros, in vehicula velit varius in. Curabitur eget commodo tortor. Nullam ut porttitor dui. Integer eget velit sed est lacinia fermentum eget a leo. Phasellus ex tellus, tincidunt ut sagittis sed, lacinia in lectus. Nulla vitae maximus enim, eu aliquam tortor. Mauris quam odio, aliquet quis dolor a, pellentesque vehicula enim. Phasellus tortor mi, consectetur sit amet pulvinar et, sollicitudin at tortor. Praesent ac est eget est sodales pretium sed eget turpis. Suspendisse elementum, ante nec feugiat faucibus, nulla dolor lacinia lorem, id commodo mauris tellus nec ligula. Mauris non augue sed arcu porta placerat.', 0, '2018-08-04 11:20:00'),
        new model.Issue(5, 'Vestibulum rhoncus purus eros, in vehicula velit varius in', 'Vestibulum non dui sed quam fringilla aliquet. Suspendisse potenti. Vivamus gravida lacus ut semper lobortis. Suspendisse aliquam maximus quam ac convallis. Curabitur at semper ligula. Vivamus neque ante, blandit rutrum aliquam et, blandit tempor nisl. Phasellus quis est ut ante tristique aliquam at in diam. Quisque suscipit pharetra velit et tristique.', 0, '2018-08-04 11:20:00'),
        new model.Issue(6, 'Vestibulum non dui sed quam fringilla aliquet', 'Suspendisse accumsan placerat suscipit. Phasellus non tincidunt dolor. Donec bibendum ultrices ligula ac euismod. Nam consequat nunc tortor, nec euismod ligula hendrerit in. Aenean bibendum feugiat efficitur. Vivamus cursus magna a metus porttitor iaculis. In congue quis ipsum ut tincidunt. Vivamus vehicula pellentesque commodo. Donec eleifend, dolor at vulputate fermentum, elit dolor tempus nunc, condimentum egestas felis sem eu diam. Sed et imperdiet mauris. Sed quam eros, varius quis eros vitae, consequat finibus tortor.', 0, '2018-08-04 11:20:00')
    ];

    this.getAll = function() {
        return this.issues;
    };

    this.getOne = function(id) {
        return this.issues.find((issue) => {
            return issue.id === id;
        });
    };

    this.create = function(obj) {
        if (this.validate(obj)) {
            const id = this.getNextId();
            const issue = new model.Issue(
                id,  obj.title, obj.description, obj.state, moment().format('Y-MM-DD HH:mm:ss')
            );
            this.issues.push(issue);
            return issue;
        }
        return false;
    };

    this.update = function(id, obj) {
        let issue = this.getOne(id);
        if (this.validate(obj) && issue) {
            issue.title = obj.title;
            issue.description = obj.description;
            issue.state = obj.state;
            return issue;
        }
        return false;
    };

    this.delete = function(id) {
        let issue = this.getOne(id);
        if (issue) {
            this.issues = this.issues.filter(o => {
                return o.id !== id;
            });
            return true;
        }
        return false;
    };

    this.getNextId = function() {
        return Math.max(...this.issues.map(o => o.id), 0) + 1;
    };

    this.validate = function(obj) {
        // todo: validate input object
        return true;
    }
};