module.exports.Issue = function(id, title, description, state, createdAt) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.state = state;
    this.createdAt = createdAt;
};